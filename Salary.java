import java.util.Scanner;
import java.sql.*;

public class Salary {
    Scanner scanner = new Scanner(System.in);

    public void insert() {
        int id = scanner.nextInt();
        int salary = scanner.nextInt();
        String sql = "INSERT INTO salary (id, salary) VALUES (?, ?)";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root", "root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setInt(2, salary);

            int rows = statement.executeUpdate();

            if (rows > 0) {
                System.out.println("A new salary has been inserted");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public void select() {
        String sql = "SELECT * FROM salary";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()){
                System.out.println(result.getInt(1) + " " + result.getInt(2));
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void update() {
        String sql = "UPDATE salary SET salary=? WHERE id=?";
        int id = scanner.nextInt();
        int salary = scanner.nextInt();

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,id);
            statement.setInt(1,salary);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The salary has been updated");
            }

            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }

    public void delete() {
        int id = scanner.nextInt();
        String sql = "DELETE FROM salary WHERE id=?";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The salary has been deleted");
            }


            connection.close();

        } catch (SQLException ex) {
            ex.printStackTrace() ;
        }
    }
}
