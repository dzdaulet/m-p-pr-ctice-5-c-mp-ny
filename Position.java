import java.sql.*;
import java.util.Scanner;

public class Position {

    Scanner scanner = new Scanner(System.in);

    public void insert() throws ClassNotFoundException, SQLException {
        int id = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        String sql = "INSERT INTO positions (id, position_name) VALUES (?, ?)";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root", "root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setString(2, name);

            int rows = statement.executeUpdate();

            if (rows > 0) {
                System.out.println("A new position has been inserted");
            }

            connection.close();

        } catch (SQLException exce) {
            exce.printStackTrace();
        }

    }

    public void select() {
        String sql = "SELECT * FROM positions";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery(sql);

            while (result.next()){
                System.out.println(result.getInt(1) + " " + result.getString(2));
            }

            connection.close();

        } catch (SQLException exce) {
            exce.printStackTrace() ;
        }
    }

    public void update() {
        int id = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        String sql = "UPDATE positions SET name=? WHERE id=?";

        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1,name);
            statement.setInt(2,id);
            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The position has been updated");
            }

            connection.close();

        } catch (SQLException exce) {
            exce.printStackTrace() ;
        }
    }

    public void delete() {
        int id = scanner.nextInt();
        String sql = "DELETE FROM positions WHERE id=?";
        try {
            Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ass5", "root","root");

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);

            int rows = statement.executeUpdate();

            if(rows > 0) {
                System.out.println("The position has been deleted");
            }


            connection.close();

        } catch (SQLException exce) {
            exce.printStackTrace() ;
        }
    }
}
